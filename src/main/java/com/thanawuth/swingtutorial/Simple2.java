package com.thanawuth.swingtutorial;

import javax.swing.JButton;
import javax.swing.JFrame;

class MyFrame extends JFrame {
    JButton button;
    public MyFrame() {
        super("First JFrame");
        this.setSize(400,500);
        button = new JButton("Click");
        button.setBounds(130,100,100,40);
        this.setLayout(null);
        this.add(button);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }
}
public class Simple2 {
    public static void main(String[] args) {
        MyFrame frame = new MyFrame();
        frame.setVisible(true);
    }
}
